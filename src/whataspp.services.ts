import { Injectable } from '@nestjs/common';

const { Client, LocalAuth } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');

@Injectable()
class WhatsappService {
  client = new Client({
    authStrategy: new LocalAuth({
      dataPath: 'session',
      clientId: 'user1',
    }),
  });

  constructor() {
    console.log('starting');

    //autentifikasi via qr.
    this.client.on('qr', (qr) => {
      console.log('Silahkan scan qr di bawah ini ');
      qrcode.generate(qr, { small: true });
    });
    this.client.initialize();
  }

  kirimPesan(nomor_penerima: string, pesan: string) {
    console.log('Mengirim pesan....');
    this.client.on('ready', () => {
      console.log('Client is ready!');
      const chatId = nomor_penerima.substring(1) + '@c.us'; // menghapus tanda plus di awal
      // Sending message.
      this.client.sendMessage(chatId, pesan);
      console.log('pesan terkirim');
    });
  }

  chatBoot() {
    this.client.on('ready', () => {
      this.client.on('message', async (message) => {
        if (message.body === 'halo' || message.body === 'Halo') {
          await message.reply('halo juga umi'); // auto balas
        }
      });
    });
  }
}

export default WhatsappService;
