import { Controller, Get } from '@nestjs/common';
import WhatsappService from './whataspp.services';

@Controller()
export class AppController {
  constructor(private readonly whatsapp: WhatsappService) {}

  @Get()
  kirimPesan() {
    this.whatsapp.kirimPesan('+6282349189692', 'Halo umi saya kirim pesan');
    return { status: 'sukses' };
  }

  @Get('chat-boot')
  chatBoot() {
    //misal gunakan hp org lain untuk chat kamu.
    // ketikkan halo
    // nnti whatspap akan memblas secara otomatsi
    this.whatsapp.chatBoot();
    return { status: 'sukses', data: 'chatbot' };
  }
}
